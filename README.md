# Layrageu

[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

> Toot toot, Layrageu

[GNU/Linux](https://framagit.org/etnadji/layrageu/-/raw/master/release/Layrageu-0_2_1-x86_64) — [MacOS](https://framagit.org/etnadji/layrageu/-/raw/master/release/Layrageu_0_2_1.app)

[FR](#fr) — [EN](#en)

## FR

**Layrageu vous permet d’envoyer des fils Mastodon préparés dans LibreOffice.**

### Écrire un fil Mastodon dans LibreOffice

**Vous devez enregistrer votre fichier au Format « ODF XML plat »** 
(disponible dans la liste déroulante lors de l’enregistrement).

Si vous utilisez l’interface standard : activez la barre d’outils 
**Formatage (Styles)**.

Si vous utilisez une interface alternative : repérez les boutons 
**Accentuation**, **Accentuation forte**.

Il est possible de forcer le passage à un nouveau pouet, en écrivant un 
paragraphe composé de traits d’union : `--`.

Pour écrire de manière plus compacte, ajoutez des colonnes à la mise en page 
via **Format** &gt; **Colonnes**. Les sauts de colonnes n’ont aucune conséquence.

### Envoyer un fil avec Layrageu

Layrageu se lance avec via l’application ou la commande `layrageu-gui` 
(installation via PyPI).

### Envoyer un fil avec Layrageu (ligne de commande)

Layrageu se lance dans un terminal avec la commande `layrageu`.

## EN

**Layrageu allows you to send Mastodon threads prepared inside LibreOffice**

### How to write a Mastodon thread inside LibreOffice

**You must save your file in the “Flat ODF XML” format**
(available in the format combobox in the save window).

### How to send a Mastodon thread with Layrageu

Starts Layrageu with the binary application ou the `layrageu-gui` command 
(PyPI installation).

### How to send a Mastodon thread with Layrageu (command line)

Starts Layrageu command line interface with `layrageu` command.

## PyPI

```bash
pip3 install layrageu
```

<!-- vim:set spl=fr,en: -->
