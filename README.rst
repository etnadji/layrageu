========
Layrageu
========

**Layrageu allows you to send Mastodon threads prepared inside LibreOffice.**

`GNU/Linux`_ — `MacOS`_

.. _MacOS: https://framagit.org/etnadji/layrageu/-/raw/master/release/Layrageu_0_2_1.app
.. _GNU/Linux: https://framagit.org/etnadji/layrageu/-/raw/master/release/Layrageu-0_2_1-x86_64

