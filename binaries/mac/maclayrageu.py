#!/usr/bin/python3
# -*- coding:Utf-8 -*-

"""
Layrageu - Script pour PyInstaller (macOS)
"""

from layrageu.gui import main

__author__ = "Étienne Nadji <etnadji@eml.cc>"

if __name__ == '__main__':
    main()

# vim:set shiftwidth=4 softtabstop=4:
