# Changelog

[Last published version](#02-2022-11-26)

### TODO

#### Features from Mastodon.py (misc)

[ ] Sender : Mastodon.py : Handle `scheduled_at` option in Mastodon.py `status_post()`.

> Pass a datetime as `scheduled_at` to schedule the toot for a specific time
> (the time must be at least 5 minutes into the future). If this is passed,
> `status_post` returns a scheduled toot dict instead.

[ ] Sender : Mastodon.py : Handle `language` option in Mastodon.py `status_post()` using iso639-lang library.

> Specify `language` to override automatic language detection. The parameter 
> accepts all valid ISO 639-2 language codes.

#### Features from Mastodon.py (media post)

[ ] Sender : Mastodon.py : `media_post()` and `media_ids` in `status_post()`.
[ ] Loader : Handle locale linked pictures in FODT (**mandatory for** `media-post()`).

### UIs configuration

[ ] GUI : Settings window

#### Shared settings

[ ] GUI : Auto-check “Sensitive” Checkbutton if instance toots are always sensitive.
[ ] GUI : Configure instances to always mark toots as sensitive.

## 0.2.1 — 2022-11-27

**This version fixes sensitive posts in GUI.**

### Sensitive posts

Toots were not **posted** as sensitive with the GUI.

- Add a new setting to always mark toots as sensitive, per-instance.
- The said setting can only be set with TUI for now.

## 0.2 — 2022-11-26

### Sensitive posts

It is now possible to mark toots as sensitive.

- GUI: Check “Sensitive” box.
- TUI: Use `--sensitive` option.

### Multi-instance management

It is now possible to choose which instance to post to.

- GUI: Change the “Send on instance” combobox to select the instance.
- GUI: Create a new instance with **Instances > New**.

- TUI: Use `--instance=<instance_name>` option.
- TUI: Without `--instance` option, the instance called `default` is always used.

### UIs configuration

This release change how settings are stored (but the folder used is still the same).

Instances settings are moved from `settings.ini` to `instances.ini`.

### Miscellaneous

Command-line interface has a `version` command. It displays Layrageu's version. How original.

## 0.1 — 2022-11-15

First version.

<!-- vim:set ft=markdown spl=en: -->
