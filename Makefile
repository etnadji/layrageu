.PHONY: format black releases pip pip-reload gnupkg gnuspec macpkg macspec macpkgicon pypi pypi-test sphinx

format:
	black -l 79 layrageu/*.py

black: format

releases:
	@echo "--- GNU/Linux -----------------------------------"
	-cp dist/GnuLayrageu releases/

pip: format
	python3 setup.py sdist bdist_wheel

pip-reload:
	-pip3 uninstall layrageu
	pip3 install dist/layrageu-0.2-py3-none-any.whl

gnupkg:
	python3 -m PyInstaller gnu.spec --noconfirm

gnuspec:
	python3 -m PyInstaller binaries/gnu/gnulayrageu.py --name Layrageu --collect-data layrageu --onefile --windowed -i binaries/gnu/icon.png
	mv Layrageu.spec gnu.spec

macpkg:
	python3 -m PyInstaller mac.spec --noconfirm

macspec:
	python3 -m PyInstaller binaries/mac/maclayrageu.py --name Layrageu --collect-data layrageu --onefile --windowed -i binaries/mac/icon.icns
	mv Layrageu.spec mac.spec

macpkgicon:
	makeicns -512 binaries/mac/icon.png -256 binaries/mac/icon.png -128 binaries/mac/icon.png -32 binaries/mac/icon.png -16 binaries/mac/icon.png -out binaries/mac/icon.icns

pypi-test:
	-mkdir tmp
	-mv dist/Layrageu.app tmp/
	-mv dist/Layrageu tmp/
	python3 -m twine upload --repository testpypi dist/*
	-mv tmp/* dist/
	rmdir tmp

pypi:
	-mkdir tmp
	-mv dist/Layrageu.app tmp/
	-mv dist/Layrageu tmp/
	python3 -m twine upload --repository pypi dist/*
	-mv tmp/* dist/
	rmdir tmp

sphinx:
	sphinx-build -b html doc/ build/doc/
